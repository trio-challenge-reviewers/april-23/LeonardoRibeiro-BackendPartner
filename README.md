# Auction System
This project is an implementation of an auto-bid auction system that allows bidders to pre-authorize bids up to a specified maximum amount, even when they are not actively connected to the platform. 
The system includes a simple Java API designed to be easily integrated into the auction house's website by other developers working on the project. The API handles auction processing, bid management, and winner determination, offering a flexible and extensible solution to meet the evolving needs of the auction house.

* [API EXPLANATORY VIDEO] (https://drive.google.com/file/d/1H525QsL_ZgQs3C0P1aMEGaE7sCyVSnvN/view?usp=share_link)

* [BONUS - SpringBoot project] (https://gitlab.com/trio-challenge-reviewers/april-23/LeonardoRibeiro-Bonus)


# Scenarios
Bidders are listed in the order they entered their information on the site. Should there be a tie between two or more bidders, the first person that entered their information wins. The amount of the winner's bid should be the lowest amount possible while observing all the previous rule. The following json is an example of a possible auction of a item. 

``` json
{
"item": "Notebook",
"bidders": [
 {        "name": "Alicia",        "startingBid": 50.00,        "maxBid": 80.00,        "incrementAmount": 3.00},
 {        "name": "Olivia",        "startingBid": 60.00,        "maxBid": 80.00,        "incrementAmount": 2.00},
 {        "name": "Mason",        "startingBid": 55.00,        "maxBid": 80.00,        "incrementAmount": 5.00}
]
}
```

# Thought Process
Our main goal was to create a simple, easy-to-understand auction processing system, adhering to best practices such as the SOLID principles and Clean Code guidelines. We aimed for a design that is flexible, maintainable, and scalable, incorporating relevant design patterns as appropriate.

# API Design
The API consists of the following main components:

- AuctionItem: Represents an item being auctioned, containing a list of bidders.
- Bidder: Represents a person bidding on the auction, including their starting bid, max bid, and increment amount.
- AuctionService: Orchestrates the auction process by utilizing an implementation of IAuctionProcessor.
- IAuctionProcessor: Interface for processing auction logic, allowing for different strategies.
- DefaultAuctionProcessor: A specific implementation of IAuctionProcessor using a simple Java approach.
- AuctionProcessorFactory: A factory pattern implementation for creating instances of IAuctionProcessor.
- AuctionLogger: A utility class that logs important events during the auction process for tracking and debugging purposes.
- AuctionProcessorTest: Test class with a diverse set of test cases, covering both typical use cases and edge cases, which thoroughly validate the auction processing logic. This includes scenarios with various bidder numbers, bid amounts, and increments, as well as cases with no valid bidders.

# Trade-offs
We opted for a simple Java implementation in the DefaultAuctionProcessor, which provides a clear and easy-to-follow solution without introducing unnecessary complexity. Our goal was to keep the system as simple as possible, considering that the auction processing in this implementation is not designed for real-time processing.

However, this choice might have some trade-offs:

- Limited real-time updates: By not implementing patterns like the Observer, components interested in auction events might not receive real-time updates, potentially affecting the user experience and responsiveness of the system.

- Potential scalability issues: In a large-scale, real-world scenario with many concurrent auctions, the current implementation may not offer the best performance or scalability.

Despite these trade-offs, we chose to prioritize simplicity in the initial implementation. If real-time updates and improved scalability become essential requirements in the future, enhancements such as the Observer pattern could be considered to improve the auction processing logic.


# Overall Approach

During the development of our auction system, we made sure to adhere to best practices such as SOLID principles and Clean Code guidelines, which helped us build a maintainable and readable solution.

To facilitate the flexibility and extensibility of our auction processing strategies, we employed the Factory Method pattern. This approach allows us to create instances of IAuctionProcessor easily and customize them as needed.

We've also implemented the Dependency Injection pattern to promote loose coupling between the AuctionService and IAuctionProcessor. This design choice ensures that our system remains modular and adaptable to future changes.

By incorporating the Strategy pattern, we've managed to encapsulate different auction processing algorithms. This enables us to add or modify processing strategies without affecting the existing codebase, further improving the system's maintainability.

Finally, we have created a comprehensive suite of unit tests to ensure the correctness and reliability of our implementation. These tests help us maintain high code quality and provide a safety net when implementing new features or refactoring the existing code.

In summary, our approach to building the auction system is focused on flexibility, maintainability, and scalability, employing proven design patterns and best practices to create a robust and adaptable solution.


# Usage
To use the auction system, get the file auction.jar from bin folder and add it to your java project. Create an instance of AuctionService with an appropriate implementation of IAuctionProcessor, and call the runAuction() method with an AuctionItem object.

``` java
List<Bidder> bidders = Arrays.asList(new Bidder("Alicia", 50.00, 80.00, 3.00),
					new Bidder("Olivia", 60.00, 82.00, 2.00), new Bidder("Mason", 55.00, 185.00, 5.00));
AuctionItem auctionItem = new AuctionItem("Skates", bidders);
AuctionService auctionService = new AuctionService(
					AuctionProcessorFactory.createDefaultProcessor("auction.log"));
Optional<AuctionResult> result = auctionService.runAuction(auctionItem);
System.out.println("Winning Bidder: " + result.get().getWinner().getName());
System.out.println("Winning Bid Amount: " + result.get().getWinner().getCurrentBid());

```

This will process the auction and return the result, including the winning bidder and their final bid.

# Logging and Auditing
We implemented a logging mechanism for auditing purposes. The auction system logs significant events, such as the start and end of an auction, and the resulting winner. This log data can be useful for troubleshooting, monitoring, or reviewing past auctions.

``` 
2023-04-24T07:56:58.285194 - ------------------------------------
2023-04-24T07:56:58.285194 - Starting auction for item: Skates
2023-04-24T07:56:58.285194 - ------------------------------------
2023-04-24T07:56:58.285194 - Alicia placed a bid of 50.0
2023-04-24T07:56:58.286194 - Olivia placed a bid of 52.0
2023-04-24T07:56:58.286194 - Alicia placed a bid of 55.0
2023-04-24T07:56:58.286194 - Olivia placed a bid of 57.0
2023-04-24T07:56:58.287193 - Alicia placed a bid of 60.0
2023-04-24T07:56:58.287193 - Olivia placed a bid of 62.0
2023-04-24T07:56:58.287193 - Alicia placed a bid of 65.0
2023-04-24T07:56:58.287193 - Olivia placed a bid of 67.0
2023-04-24T07:56:58.288193 - Alicia placed a bid of 70.0
2023-04-24T07:56:58.288193 - Olivia placed a bid of 72.0
2023-04-24T07:56:58.288193 - Alicia placed a bid of 75.0
2023-04-24T07:56:58.288193 - Olivia placed a bid of 77.0
2023-04-24T07:56:58.289343 - Alicia placed a bid of 80.0
2023-04-24T07:56:58.289343 - Olivia placed a bid of 82.0
2023-04-24T07:56:58.290193 - Auction ended for item: Skates
2023-04-24T07:56:58.290193 - ++++++++++++++++++++++++++++++++++++++++++
2023-04-24T07:56:58.290193 - Winning Bidder: Olivia
2023-04-24T07:56:58.291189 - Winning Bid Amount: 82.0
2023-04-24T07:56:58.291189 - ++++++++++++++++++++++++++++++++++++++++++
``` 



# Future Enhancements
- Implement alternative auction processing strategies for improved performance or different rules.
- Add more validation and error handling to ensure robustness and data integrity.
- Improve logging and auditing capabilities to provide more insights into the system's operation.
