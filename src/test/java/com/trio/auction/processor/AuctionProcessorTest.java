package com.trio.auction.processor;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.trio.auction.model.AuctionItem;
import com.trio.auction.model.AuctionResult;
import com.trio.auction.model.Bidder;
import com.trio.auction.service.AuctionService;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * Test class with a diverse set of test cases, covering both typical use cases and edge cases, 
 * which thoroughly validate the auction processing logic. This includes scenarios with various 
 * bidder numbers, bid amounts, and increments, as well as cases with no valid bidders.
 */
class AuctionProcessorTest {

	private AuctionService auctionService;

	@BeforeEach
	void setUp() {
		auctionService = new AuctionService(AuctionProcessorFactory.createDefaultProcessor("auction.log"));
	}

	@Test
	void testAuctionWithMultipleBidders() {
		List<Bidder> bidders = Arrays.asList(new Bidder("Alicia", 50.00, 80.00, 3.00),
				new Bidder("Olivia", 60.00, 82.00, 2.00), new Bidder("Mason", 55.00, 85.00, 5.00));
		AuctionItem auctionItem = new AuctionItem("Skates", bidders);

		Optional<AuctionResult> result = auctionService.runAuction(auctionItem);
		assertEquals("Mason", result.get().getWinner().getName());
		assertEquals(85.00, result.get().getWinner().getCurrentBid());
	}
	
	@Test
	void testAuctionWithFiveBiddersDifferentMaxBids() {
	    List<Bidder> bidders = Arrays.asList(
	            new Bidder("Alicia", 50.00, 80.00, 3.00),
	            new Bidder("Olivia", 55.00, 85.00, 2.00),
	            new Bidder("Mason", 60.00, 90.00, 5.00),
	            new Bidder("Sophia", 45.00, 75.00, 4.00),
	            new Bidder("Ethan", 65.00, 95.00, 6.00));
	    AuctionItem auctionItem = new AuctionItem("Skates", bidders);

	    Optional<AuctionResult> result = auctionService.runAuction(auctionItem);
	    assertEquals("Ethan", result.get().getWinner().getName());
	    assertEquals(94.00, result.get().getWinner().getCurrentBid());
	}
	
	@Test
	void testAuctionWithFiveBiddersSameMaxBidsAndDifferentStartingBids() {
	    List<Bidder> bidders = Arrays.asList(
	            new Bidder("Alicia", 50.00, 100.00, 3.00),
	            new Bidder("Olivia", 55.00, 100.00, 2.00),
	            new Bidder("Mason", 60.00, 100.00, 5.00),
	            new Bidder("Sophia", 45.00, 100.00, 4.00),
	            new Bidder("Ethan", 65.00, 100.00, 6.00));
	    AuctionItem auctionItem = new AuctionItem("Skates", bidders);

	    Optional<AuctionResult> result = auctionService.runAuction(auctionItem);
	    assertEquals("Sophia", result.get().getWinner().getName());
	    assertEquals(99.00, result.get().getWinner().getCurrentBid());
	}

	@Test
	void testAuctionWithSingleBidder() {
		List<Bidder> bidders = Arrays.asList(new Bidder("Alicia", 50.00, 80.00, 3.00));
		AuctionItem auctionItem = new AuctionItem("Skates", bidders);

		Optional<AuctionResult> result = auctionService.runAuction(auctionItem);
		assertEquals("Alicia", result.get().getWinner().getName());
		assertEquals(50.00, result.get().getWinner().getCurrentBid());
	}

	@Test
	void testAuctionWithNoBids() {
		List<Bidder> bidders = Arrays.asList();
		AuctionItem auctionItem = new AuctionItem("Skates", bidders);
		Optional<AuctionResult> result = auctionService.runAuction(auctionItem);
		assertFalse(result.isPresent());
	}

	@Test
	void testAuctionWithIncrementLargerThanMaxBid() {
		List<Bidder> bidders = Arrays.asList(new Bidder("Alicia", 50.00, 80.00, 100.00));
		AuctionItem auctionItem = new AuctionItem("Skates", bidders);

		Optional<AuctionResult> result = auctionService.runAuction(auctionItem);
		assertEquals("Alicia", result.get().getWinner().getName());
		assertEquals(50.00, result.get().getWinner().getCurrentBid());
	}

	@Test
	void testAuctionWithAllBiddersHavingSameMaxBid() {
		List<Bidder> bidders = Arrays.asList(new Bidder("Alicia", 50.00, 80.00, 3.00),
				new Bidder("Olivia", 60.00, 80.00, 2.00), new Bidder("Mason", 55.00, 80.00, 5.00));
		AuctionItem auctionItem = new AuctionItem("Skates", bidders);

		Optional<AuctionResult> result = auctionService.runAuction(auctionItem);
		assertEquals("Olivia", result.get().getWinner().getName());
		assertEquals(80.00, result.get().getWinner().getCurrentBid());
	}

	@Test
	void testAuctionWithTwoBiddersReachingMaxBidAtTheSameTime() {
		List<Bidder> bidders = Arrays.asList(new Bidder("Alicia", 50.00, 80.00, 5.00),
				new Bidder("Olivia", 55.00, 80.00, 5.00));
		AuctionItem auctionItem = new AuctionItem("Skates", bidders);

		Optional<AuctionResult> result = auctionService.runAuction(auctionItem);
		assertEquals("Alicia", result.get().getWinner().getName());
		assertEquals(80.00, result.get().getWinner().getCurrentBid());
	}

	@Test
	void testAuctionWithBiddersHavingSameStartingBid() {
		List<Bidder> bidders = Arrays.asList(new Bidder("Alicia", 50.00, 80.00, 3.00),
				new Bidder("Olivia", 50.00, 85.00, 2.00));
		AuctionItem auctionItem = new AuctionItem("Skates", bidders);

		Optional<AuctionResult> result = auctionService.runAuction(auctionItem);
		assertEquals("Olivia", result.get().getWinner().getName());
		assertEquals(82.00, result.get().getWinner().getCurrentBid());
	}

	@Test
	void testAuctionWithIncrementEqualToMaxBid() {
		List<Bidder> bidders = Arrays.asList(new Bidder("Alicia", 50.00, 80.00, 80.00));
		AuctionItem auctionItem = new AuctionItem("Skates", bidders);

		Optional<AuctionResult> result = auctionService.runAuction(auctionItem);
		assertEquals("Alicia", result.get().getWinner().getName());
		assertEquals(50.00, result.get().getWinner().getCurrentBid());
	}

	@Test
	void testAuctionWithNoWinnerDueToStartingBidHigherThanMaxBid() {
		List<Bidder> bidders = Arrays.asList(new Bidder("Alicia", 90.00, 80.00, 3.00),
				new Bidder("Olivia", 85.00, 80.00, 2.00));
		AuctionItem auctionItem = new AuctionItem("Skates", bidders);

		Optional<AuctionResult> result = auctionService.runAuction(auctionItem);
		assertFalse(result.isPresent());
	}

	@Test
	void testAuctionWithMultipleBiddersWithSameMaxBid() {
		List<Bidder> bidders = Arrays.asList(new Bidder("Alicia", 50.00, 80.00, 3.00),
				new Bidder("Olivia", 55.00, 80.00, 2.00), new Bidder("Mason", 60.00, 80.00, 5.00));
		AuctionItem auctionItem = new AuctionItem("Skates", bidders);

		Optional<AuctionResult> result = auctionService.runAuction(auctionItem);
		assertEquals("Mason", result.get().getWinner().getName());
		assertEquals(80.00, result.get().getWinner().getCurrentBid());
	}

	@Test
	void testAuctionWithBiddersHavingSameMaxBidAndIncrement() {
		List<Bidder> bidders = Arrays.asList(new Bidder("Alicia", 50.00, 80.00, 5.00),
				new Bidder("Olivia", 55.00, 80.00, 5.00));
		AuctionItem auctionItem = new AuctionItem("Skates", bidders);

		Optional<AuctionResult> result = auctionService.runAuction(auctionItem);
		assertEquals("Alicia", result.get().getWinner().getName());
		assertEquals(80.00, result.get().getWinner().getCurrentBid());
	}

	@Test
	void testAuctionWithBiddersStartingWithMaxBid() {
		List<Bidder> bidders = Arrays.asList(new Bidder("Alicia", 80.00, 80.00, 3.00),
				new Bidder("Olivia", 75.00, 85.00, 2.00));
		AuctionItem auctionItem = new AuctionItem("Skates", bidders);

		Optional<AuctionResult> result = auctionService.runAuction(auctionItem);
		assertEquals("Olivia", result.get().getWinner().getName());
		assertEquals(82.00, result.get().getWinner().getCurrentBid());
	}

	@Test
	void testAuctionWithBiddersHavingStartingBidEqualToMaxBid() {
		List<Bidder> bidders = Arrays.asList(new Bidder("Alicia", 80.00, 80.00, 3.00),
				new Bidder("Olivia", 80.00, 80.00, 2.00));
		AuctionItem auctionItem = new AuctionItem("Skates", bidders);

		Optional<AuctionResult> result = auctionService.runAuction(auctionItem);
		assertEquals("Alicia", result.get().getWinner().getName());
		assertEquals(80.00, result.get().getWinner().getCurrentBid());
	}
	
	@Test
	void testAuctionWithMaxBidLessThanStartingBid() {
	    List<Bidder> bidders = Arrays.asList(new Bidder("Alicia", 50.00, 45.00, 3.00));
	    AuctionItem auctionItem = new AuctionItem("Skates", bidders);

	    Optional<AuctionResult> result = auctionService.runAuction(auctionItem);
	    assertFalse(result.isPresent());
	}

	@Test
	void testAuctionWithNegativeStartingBid() {
	    List<Bidder> bidders = Arrays.asList(new Bidder("Alicia", -10.00, 80.00, 3.00));
	    AuctionItem auctionItem = new AuctionItem("Skates", bidders);

	    Optional<AuctionResult> result = auctionService.runAuction(auctionItem);
	    assertFalse(result.isPresent());
	}

	@Test
	void testAuctionWithNegativeMaxBid() {
	    List<Bidder> bidders = Arrays.asList(new Bidder("Alicia", 50.00, -80.00, 3.00));
	    AuctionItem auctionItem = new AuctionItem("Skates", bidders);

	    Optional<AuctionResult> result = auctionService.runAuction(auctionItem);
	    assertFalse(result.isPresent());
	}

	@Test
	void testAuctionWithNegativeIncrement() {
	    List<Bidder> bidders = Arrays.asList(new Bidder("Alicia", 50.00, 80.00, -3.00));
	    AuctionItem auctionItem = new AuctionItem("Skates", bidders);

	    Optional<AuctionResult> result = auctionService.runAuction(auctionItem);
	    assertFalse(result.isPresent());
	}

	@Test
	void testAuctionWithZeroIncrement() {
	    List<Bidder> bidders = Arrays.asList(new Bidder("Alicia", 50.00, 80.00, 0.00));
	    AuctionItem auctionItem = new AuctionItem("Skates", bidders);
	    Optional<AuctionResult> result = auctionService.runAuction(auctionItem);
	    assertFalse(result.isPresent());
	}

	@Test
	void testAuctionWithDecimalIncrement() {
	    List<Bidder> bidders = Arrays.asList(new Bidder("Alicia", 50.00, 80.00, 0.50));
	    AuctionItem auctionItem = new AuctionItem("Skates", bidders);

	    Optional<AuctionResult> result = auctionService.runAuction(auctionItem);
	    assertEquals("Alicia", result.get().getWinner().getName());
	    assertEquals(50.00, result.get().getWinner().getCurrentBid());
	}

	@Test
	void testAuctionWithSingleBidderHavingStartingBidEqualToMaxBid() {
	    List<Bidder> bidders = Arrays.asList(new Bidder("Alicia", 80.00, 80.00, 3.00));
	    AuctionItem auctionItem = new AuctionItem("Skates", bidders);

	    Optional<AuctionResult> result = auctionService.runAuction(auctionItem);
	    assertEquals("Alicia", result.get().getWinner().getName());
	    assertEquals(80.00, result.get().getWinner().getCurrentBid());
	}

    @Test
    void testAuctionWithSingleBidderHavingIncrementGreaterThanMaxBid() {
        List<Bidder> bidders = Arrays.asList(new Bidder("Alicia", 50.00, 80.00, 100.00));
        AuctionItem auctionItem = new AuctionItem("Skates", bidders);

        Optional<AuctionResult> result = auctionService.runAuction(auctionItem);
        assertEquals("Alicia", result.get().getWinner().getName());
        assertEquals(50.00, result.get().getWinner().getCurrentBid());
    }

    @Test
    void testAuctionWithTwoBiddersStartingBelowMaxBidAndReachingMaxBid() {
        List<Bidder> bidders = Arrays.asList(new Bidder("Alicia", 50.00, 80.00, 10.00),
                new Bidder("Olivia", 55.00, 85.00, 10.00));
        AuctionItem auctionItem = new AuctionItem("Skates", bidders);

        Optional<AuctionResult> result = auctionService.runAuction(auctionItem);
        assertEquals("Olivia", result.get().getWinner().getName());
        assertEquals(75.00, result.get().getWinner().getCurrentBid());
    }
    
    @Test
    void testAuctionWithTwoBiddersAndFirstOneWinningWithaLowerMaxBid() {
        List<Bidder> bidders = Arrays.asList(new Bidder("Pedro", 600.00, 7950.00, 50.00),
                new Bidder("Olivia", 500.00, 8000.00, 100.00));
        AuctionItem auctionItem = new AuctionItem("Bike", bidders);

        Optional<AuctionResult> result = auctionService.runAuction(auctionItem);
        assertEquals("Pedro", result.get().getWinner().getName());
        assertEquals(7950.00, result.get().getWinner().getCurrentBid());
    }

    @Test
    void testAuctionWithBiddersHavingMaxBidLessThanStartingBid() {
        List<Bidder> bidders = Arrays.asList(new Bidder("Alicia", 90.00, 80.00, 3.00),
                new Bidder("Olivia", 85.00, 80.00, 2.00), new Bidder("Mason", 100.00, 80.00, 5.00));
        AuctionItem auctionItem = new AuctionItem("Skates", bidders);

        Optional<AuctionResult> result = auctionService.runAuction(auctionItem);
        assertFalse(result.isPresent());
    }


}
