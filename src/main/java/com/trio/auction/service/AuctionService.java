package com.trio.auction.service;

import java.util.Optional;

import com.trio.auction.model.AuctionItem;
import com.trio.auction.model.AuctionResult;
import com.trio.auction.processor.IAuctionProcessor;

/**
 * Orchestrates the auction process by utilizing an implementation of IAuctionProcessor. 
 * This service acts as the main entry point for auction processing, allowing us to decouple 
 * the processing logic from the rest of the system.
 */
public class AuctionService {
	private IAuctionProcessor auctionProcessor;

	/**
	 * Constructs a new {@link AuctionService} using the given
	 * {@link IAuctionProcessor}.
	 *
	 * @param auctionProcessor the auction processor to be used for processing
	 *                         auctions
	 */
	public AuctionService(IAuctionProcessor auctionProcessor) {
		this.auctionProcessor = auctionProcessor;
	}

	/**
	 * Runs the auction process for the given auction item using the provided auction processor.
	 * <p>
	 * This method delegates the processing of the auction to the auctionProcessor instance
	 * and returns an Optional<AuctionResult> object.
	 * </p>
	 *
	 * @param auctionItem the {@code AuctionItem} object containing the item and the
	 *                    list of bidders participating in the auction
	 * @return an {@code Optional<AuctionResult>} containing the auction result if a
	 *         winner is determined, otherwise an empty Optional
	 */
	public Optional<AuctionResult> runAuction(AuctionItem auctionItem) {
		return auctionProcessor.processAuction(auctionItem);
	}
}
