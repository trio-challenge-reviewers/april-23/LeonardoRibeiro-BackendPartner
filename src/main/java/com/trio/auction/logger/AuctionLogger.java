package com.trio.auction.logger;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import com.trio.auction.model.Bidder;

/**
 * A logger for auction events, capable of writing log messages to a specified file.
 */
public class AuctionLogger {
	
	private final String logFilename;

	public AuctionLogger(String logFilename) {
		this.logFilename = logFilename;
	}

	public void info(String message) {
		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSSSS");
		String formattedDateTime = now.format(formatter);

		try (BufferedWriter writer = new BufferedWriter(new FileWriter(logFilename, true))) {
			writer.write(formattedDateTime + " - " + message);
			writer.newLine();
		} catch (IOException e) {
			System.err.println("Error writing log: " + e.getMessage());
		}
	}

	public void logAuctionStart(String itemName) {
		info("------------------------------------");
		info("Starting auction for item: " + itemName);
		info("------------------------------------");
	}

	public void logAuctionFinish(Bidder winner) {
		info("++++++++++++++++++++++++++++++++++++++++++");
		info("Winning Bidder: " + winner.getName());
		info("Winning Bid Amount: " + winner.getCurrentBid());
		info("++++++++++++++++++++++++++++++++++++++++++");
	}

	public void logNoBids() {
		info("???????????????????????????????????????????");
		info("NO BIDS!!!");
		info("???????????????????????????????????????????");
	}

}
