package com.trio.auction.model;

/**
 * Represents a person bidding on the auction, 
 * including their starting bid, max bid, and increment amount. 
 */
public class Bidder {
	
    private String name;
    private double startingBid;
    private double maxBid;
    private double incrementAmount;
    private double currentBid;

    /**
     * Constructs a new {@link Bidder} with the given bidding information.
     *
     * @param name the name of the bidder
     * @param startingBid the starting bid amount for the bidder
     * @param maxBid the maximum bid amount the bidder is willing to pay
     * @param incrementAmount the bid increment amount for the bidder
     */
    public Bidder(String name, double startingBid, double maxBid, double incrementAmount) {
        this.name = name;
        this.startingBid = startingBid;
        this.maxBid = maxBid;
        this.incrementAmount = incrementAmount;
        this.currentBid = startingBid;
    }
    
    public Bidder() {
		super();
	}

	public String getName() {
        return name;
    }

    public double getStartingBid() {
        return startingBid;
    }

    public double getMaxBid() {
        return maxBid;
    }

    public double getIncrementAmount() {
        return incrementAmount;
    }

    public double getCurrentBid() {
        return currentBid;
    }

    public void setCurrentBid(double currentBid) {
        this.currentBid = currentBid;
    }
}

