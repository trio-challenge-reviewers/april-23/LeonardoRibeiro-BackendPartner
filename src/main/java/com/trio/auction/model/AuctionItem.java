package com.trio.auction.model;

import java.util.List;

/**
 * 
 * Represents an item being auctioned, containing a list of bidders. 
 * This abstraction helps to encapsulate the details of each item and its associated bidders, 
 * making it easier to reason about the auction process.
 */
public class AuctionItem {
	private String item;
	private List<Bidder> bidders;

	/**
	 * Constructs a new {@link AuctionItem} with the given item and list of bidders.
	 *
	 * @param item    the item being auctioned
	 * @param bidders the list of {@link Bidder} participating in the auction
	 */
	public AuctionItem(String item, List<Bidder> bidders) {
		this.item = item;
		this.bidders = bidders;
	}
	
	public String getItem() {
		return item;
	}

	public List<Bidder> getBidders() {
		return bidders;
	}
}
