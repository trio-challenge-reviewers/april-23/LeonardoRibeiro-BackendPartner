package com.trio.auction.model;

/**
 * Represents the result of an auction, including the item, winner, and date.
 */
public class AuctionResult {
	private String item;
	private Bidder winner;
	private String date;

	/**
	 * Constructs a new {@link AuctionResult} with the given item, winner, and date.
	 *
	 * @param item the item being auctioned
	 * @param winner the winning {@link Bidder} for the auction
	 * @param date the date when the auction ended
	 */
	public AuctionResult(String item, Bidder winner, String date) {
		this.item = item;
		this.winner = winner;
		this.date = date;
	}

	public String getItem() {
		return item;
	}

	public Bidder getWinner() {
		return winner;
	}

	public String getDate() {
		return date;
	}
}
