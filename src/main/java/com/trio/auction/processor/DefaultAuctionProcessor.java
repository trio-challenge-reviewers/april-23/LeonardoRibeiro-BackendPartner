package com.trio.auction.processor;

import com.trio.auction.logger.AuctionLogger;
import com.trio.auction.model.AuctionItem;
import com.trio.auction.model.AuctionResult;
import com.trio.auction.model.Bidder;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * A specific implementation of IAuctionProcessor using a simple Java approach. 
 * This implementation provides a clear and easy-to-follow solution, but alternative 
 * strategies can be plugged in if needed, showcasing the power of the Strategy Pattern
 */
public class DefaultAuctionProcessor implements IAuctionProcessor {
	private final AuctionLogger logger;

	/**
	 * Constructs a new {@code DefaultAuctionProcessor} with the specified logger.
	 *
	 * @param logger the logger to be used for logging auction events
	 */
	public DefaultAuctionProcessor(AuctionLogger logger) {
		this.logger = logger;
	}

	/**
	 * Processes an auction for the given auction item and determines the winner.
	 * <p>
	 * This method takes an {@code AuctionItem} as input, which includes item details and a list of bidders.
	 * It filters valid bidders and finds the winner based on the highest bid.
	 * If there are no valid bidders or the auction has no bidders, an empty Optional is returned.
	 * </p>
	 *
	 * @param auctionItem the auction item to be processed, which includes item details and a list of bidders
	 * @return an Optional of {@code AuctionResult} containing the auction result with the winning bidder
	 *         and the final bid amount, or an empty Optional if there are no valid bidders or no bidders at all
	 */
	public Optional<AuctionResult> processAuction(AuctionItem auctionItem) {
		logger.logAuctionStart(auctionItem.getItem());

		if (auctionItem.getBidders() == null) {
			logger.logNoBids();
			return Optional.empty();
		}

		List<Bidder> validBidders = filterValidBidders(auctionItem);
		if (validBidders.isEmpty()) {
			logger.logNoBids();
			return Optional.empty();
		}

		Bidder winner = findWinner(validBidders);

		AuctionResult result = createAuctionResult(auctionItem.getItem(), winner);

		logger.logAuctionFinish(winner);

		return Optional.of(result);
	}

	/**
	 * Filters and returns a list of valid bidders for the given
	 * {@code AuctionItem}.
	 *
	 * @param auctionItem the auction item containing the bidders to be filtered
	 * @return the list of valid bidders
	 */
	private List<Bidder> filterValidBidders(AuctionItem auctionItem) {
		return auctionItem.getBidders().stream().filter(this::isValidBidder).collect(Collectors.toList());
	}

	/**
	 * Determines if the given {@code Bidder} is valid based on the specified
	 * conditions.
	 *
	 * @param bidder the bidder to be checked for validity
	 * @return {@code true} if the bidder is valid, {@code false} otherwise
	 */
	private boolean isValidBidder(Bidder bidder) {
		return 	bidder.getStartingBid() > 0 
				&& bidder.getIncrementAmount() > 0
				&& bidder.getStartingBid() <= bidder.getMaxBid() 
				&& bidder.getName() != null
				&& !bidder.getName().isEmpty();
	}

	/**
	 * Finds and returns the winner among the given list of valid bidders.
	 *
	 * @param validBidders the list of valid bidders
	 * @return the winning bidder or {@code null} if there is no winner
	 */
	private Bidder findWinner(List<Bidder> validBidders) {
		Bidder winner = null;
		double highestBid = 0;

		while (true) {
			boolean biddingEnded = true;

			for (Bidder bidder : validBidders) {
				double bid = bidder.getStartingBid();

				if (bid <= bidder.getMaxBid() && bid > highestBid) {
					highestBid = bid;
					winner = createNewBidder(bidder, bid);
					biddingEnded = false;
					logger.info(bidder.getName() + " placed a bid of " + bid);

				} else if (winner != null && !winner.getName().equals(bidder.getName())) {
					bid = highestBid + bidder.getIncrementAmount();

					if (bid <= bidder.getMaxBid()) {
						highestBid = bid;
						winner = createNewBidder(bidder, bid);
						biddingEnded = false;
						logger.info(bidder.getName() + " placed a bid of " + bid);
					}
				}
			}

			if (biddingEnded) {
				break;
			}
		}

		return winner;
	}

	/**
	 * Creates and returns a new {@code Bidder} instance based on the given original
	 * bidder and bid amount.
	 *
	 * @param originalBidder the original bidder
	 * @param bid            the bid amount
	 * @return the new bidder instance
	 */
	private Bidder createNewBidder(Bidder originalBidder, double bid) {
		return new Bidder(originalBidder.getName(), bid, originalBidder.getMaxBid(),
				originalBidder.getIncrementAmount());
	}

	/**
	 * Creates and returns an {@code AuctionResult} instance for the given item name
	 * and winning bidder.
	 *
	 * @param itemName the name of the auction item
	 * @param winner   the winning bidder
	 * @return the auction result
	 */
	private AuctionResult createAuctionResult(String itemName, Bidder winner) {
		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSSSS");
		String formattedDateTime = now.format(formatter);
		logger.info("Auction ended for item: " + itemName);
		return new AuctionResult(itemName, winner, formattedDateTime);
	}

}
