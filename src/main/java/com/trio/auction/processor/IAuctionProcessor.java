package com.trio.auction.processor;

import java.util.Optional;

import com.trio.auction.model.AuctionItem;
import com.trio.auction.model.AuctionResult;

/**
 * IAuctionProcessor defines the interface for processing auction logic.
 * <p>
 * Implementations of this interface are expected to provide specific auction
 * processing logic, which includes processing bids, determining the auction
 * winner, and generating an auction result.
 * 
 * Allows for a more flexible, maintainable, and testable system by separating the 
 * auction processing logic from the rest of the application.
 * 
 * </p>
 */
public interface IAuctionProcessor {

	Optional<AuctionResult> processAuction(AuctionItem auctionItem);
}
