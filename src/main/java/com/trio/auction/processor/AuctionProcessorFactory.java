package com.trio.auction.processor;

import com.trio.auction.logger.AuctionLogger;

/**
 * A factory pattern implementation for creating instances of IAuctionProcessor. 
 * This factory allows us to centralize the creation of processor instances, making 
 * it easier to manage and swap out different implementations.
 */
public class AuctionProcessorFactory {

	/**
	 * Creates and returns a new instance of the {@code DefaultAuctionProcessor}
	 * with the specified audit log.
	 *
	 * @param auditLog the file path for the audit log
	 * @return the new {@code DefaultAuctionProcessor} instance
	 */
	public static IAuctionProcessor createDefaultProcessor(String auditLog) {
		return new DefaultAuctionProcessor(new AuctionLogger(auditLog));

	}

}
