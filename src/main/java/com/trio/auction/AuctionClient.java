package com.trio.auction;

import com.trio.auction.model.AuctionItem;
import com.trio.auction.model.AuctionResult;
import com.trio.auction.model.Bidder;
import com.trio.auction.processor.AuctionProcessorFactory;
import com.trio.auction.service.AuctionService;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AuctionClient {

	private static final Logger logger = Logger.getLogger(AuctionClient.class.getName());

	public static void main(String[] args) {

		try {

			List<Bidder> bidders = Arrays.asList(
					new Bidder("Alicia", 50.00, 80.00, 3.00),
					new Bidder("Olivia", 60.00, 82.00, 2.00), 
					new Bidder("Mason", 55.00, 185.00, 5.00));

			AuctionItem auctionItem = new AuctionItem("Skates", bidders);

			AuctionService auctionService = new AuctionService(AuctionProcessorFactory.createDefaultProcessor("auction.log"));
			
			Optional<AuctionResult> result = auctionService.runAuction(auctionItem);
			
			System.out.println("Winning Bidder: " + result.get().getWinner().getName());
			System.out.println("Winning Bid Amount: " + result.get().getWinner().getCurrentBid());

		} catch (Exception e) {
			logger.log(Level.SEVERE, "Error processing auction", e);
		}

	}
}
